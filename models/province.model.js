const mongoose= require('mongoose')

const provinceSchema= new mongoose.Schema({
    provinceName: String,
    governor: String,
    provinceCode: String,
    provinceDescrition:String
})

const Province= mongoose.model('Province',provinceSchema)
module.exports= Province