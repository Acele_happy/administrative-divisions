const mongoose= require('mongoose')
mongoose.connect('mongodb://localhost/javascriptTest',{
    useNewUrlParser:true,
    useUnifiedTopology:true
})
.then(()=>console.log('connected to mongodb'))
.catch(err=>console.log('failed to connect'))

require('./province.model')
require('./district.model')