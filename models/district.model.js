const mongoose= require('mongoose')

const districtSchema=  new mongoose.Schema({
    districtName: {
        type: String,
        required: true
    },
    mayor: {
        type: String,
        required: true
    },
    province: {
        type: String,
        required: true
    },
    districtDescription: {
        type: String,
        required: true
    }
})

const District= mongoose.model('District',districtSchema)
module.exports= District