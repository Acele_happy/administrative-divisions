require('./models/mongodb')
const express= require('express')
const app= express()
const bodyParser= require('body-parser')
const appApis = require('morgan')

app.use(appApis ('dev'))

app.use((req,res,next)=>{
    const error= new Error('Not found');
    error.status=404
    // return next( new Error('This is an error'))
    next(error)
})

app.use(function dbErrors(error,req,res,next){
    res.status(error.status|| 500);
    res.json({
        error:{
            message: error.message
        }
    })
})




app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

const provinceController= require('./controllers/provinceController')
const districtController= require('./controllers/districtController')

app.use('/province',provinceController)
app.use('/district',districtController)

const port= process.env.PORT||4000
app.listen(port,()=>console.log(`running or port ${port}`))