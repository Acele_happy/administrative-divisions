const express= require('express')
const router= express.Router()
const Joi= require('joi')
const District= require('../models/district.model')

const districtJoiValidate= Joi.object().keys({
    districtName: Joi.string().required(),
    mayor: Joi.string().required(),
    province: Joi.string().required(),
    districtDescription: Joi.string().required()
})

router.get('/',(req,res,next)=>{
    District.find({})
    .then((district)=>res.send(district))
    .catch(err=>res.send(err))
})

router.post('/',(req,res,next)=>{
    const result= districtJoiValidate.validate(req.body)
    if(result.error){res.send(result.error.details[0].message).status(400)
    return
    }
    const {districtName,mayor,districtDescription,province}= req.body
    const newDistrict= new District({
        province,
        mayor,
        districtDescription,
        districtName
    })
    newDistrict.save()
    .then((newDistrict)=>res.send(newDistrict).status(201))
    .catch(err=>res.send(err))
})

router.delete('/:id',(req,res, next)=>{
    District.findOneAndDelete(req.params.id)
    .then(()=>res.send(`the user with id ${req.params.id} was deleted sucessfulyl`).status(400))
    .catch(err=>res.send(err))
})

router.put('/:id',(req,res,next)=>{
    const {error, value}= provinceJoiValidate.validate(req.body)
    if(error){res.send(error)
    return 0;
    }
    District.findOneAndUpdate({_id:req.params.id},value,{new:true})
    .then((updatedProvince)=>res.send(updatedProvince))
    .catch(err=> res.send(err))
})
module.exports= router