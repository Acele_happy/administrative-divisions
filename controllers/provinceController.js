const express= require('express')
const router= express.Router()
const Joi= require('joi')
const Province= require('../models/province.model')

const provinceJoiValidate= Joi.object().keys({
    provinceName: Joi.string().required('province name is required!').max(100),
    governor: Joi.string().required('Governor is required!').max(300),
    provinceCode: Joi.string().required('provinceCode name is required!').max(100),
    provinceDescription: Joi.string().required('provinceDescription name is required!').max(1000)
})

router.get('/',(req,res,next)=>{
    Province.find({})
    .then((province)=>res.send(province))
    .catch(err=>res.send(err))
})

router.post('/',(req,res,next)=>{
    const result= provinceJoiValidate.validate(req.body)
    if(result.error){res.send(result.error.details[0].message).status(400)
    return
    }
    const {provinceName,governor,provinceDescription,provinceCode}= req.body
    const newProvince= new Province({
        provinceCode,
        governor,
        provinceDescription,
        provinceName
    })
    newProvince.save()
    .then((newprovince)=>res.send(newprovince).status(201))
    .catch(err=>res.send(err))
})

router.delete('/:id',(req,res, next)=>{
    Province.findOneAndDelete(req.params.id)
    .then((deleted)=>res.send(deleted).status(400))
    .catch(err=>res.send(err))
})

router.put('/:id',(req,res,next)=>{
    const {error, value}= provinceJoiValidate.validate(req.body)
    if(error){res.send(error)
    return 0;
    }
    Province.findOneAndUpdate({_id:req.params.id},value,{new:true})
    .then((updatedProvince)=>res.send(updatedProvince))
    .catch(err=> res.send(err))
})
module.exports= router